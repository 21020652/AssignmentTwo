package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

var db *sql.DB

type Movie struct {
	MovieName string `json:"movie_name"`
	YearIntro uint64 `json:"year_intro"`
	Genre     string `json:"genre"`
}

type JsonResponse struct {
	Type    string  `json:"type"`
	Data    []Movie `json:"data"`
	Message string  `json:"message"`
}

func main() {
	cfg := mysql.Config{
		User:                 "root",
		Passwd:               "minhtom1510",
		Net:                  "tcp",
		Addr:                 "127.0.0.1:3306",
		DBName:               "Movie",
		AllowNativePasswords: true,
	}

	// Get a database handle.
	var err error
	db, err = sql.Open("mysql", cfg.FormatDSN())
	if err != nil {
		log.Fatal(err)
	}

	pingErr := db.Ping()
	if pingErr != nil {
		log.Fatal(pingErr)
	}
	fmt.Println("Connected!")

	router := mux.NewRouter()

	// Get all the movies
	//router.HandleFunc("/movies/", GetMovies).Methods("GET")

	//Create a movie
	router.HandleFunc("/movies/", CreateMovie).Methods("POST")

	// Delete a specific movie by the movieID
	router.HandleFunc("/movies/", DeleteMovie).Methods("DELETE")

	// Get one movie
	router.HandleFunc("/movies/", GetOneMovie).Methods("GET")

	// Update movie information
	router.HandleFunc("/movies/", UpdateMovie).Methods("PUT")

	// serve the app
	fmt.Println("Server at 3306")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func printMessage(message string) {
	fmt.Println("")
	fmt.Println(message)
	fmt.Println("")
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func GetMovies(w http.ResponseWriter, r *http.Request) {
	printMessage("Getting movies...")

	rows, err := db.Query("SELECT * FROM FILM")

	checkErr(err)
	var movies []Movie

	for rows.Next() {
		var movieName string
		var yearIntro uint64
		var genre string

		err = rows.Scan(&movieName, &yearIntro, &genre)

		MovieTmp := Movie{
			MovieName: movieName,
			YearIntro: yearIntro,
			Genre:     genre,
		}

		checkErr(err)
		movies = append(movies, MovieTmp)
	}

	var response = JsonResponse{Type: "success", Data: movies}

	json.NewEncoder(w).Encode(response)
}

func CreateMovie(w http.ResponseWriter, r *http.Request) {
	MovieName := r.FormValue("movie_name")
	YearIntro := r.FormValue("year_intro")
	Genre := r.FormValue("genre")

	var response = JsonResponse{}

	if MovieName == "" || Genre == "" || YearIntro == "" {
		response = JsonResponse{Type: "error", Message: "The type of MovieName and Genre is invalid"}
	} else {
		printMessage("Inserting movie into DB")
		fmt.Println("Inserting new movie with name: " + MovieName + " and it was introduced in: " + YearIntro + " and its type is: " + Genre)

		_, err := db.Exec("INSERT INTO FILM(MovieName, YearIntro, Genre) VALUES (?, ?, ?)", MovieName, YearIntro, Genre)
		//err := db.QueryRow("INSERT INTO FILM(MovieName, YearIntro, Genre) VALUES(?, ?, ?) ", MovieName, YearIntro, Genre).Scan(&lastInsertID)
		if err != nil {
			response = JsonResponse{Type: "error", Message: "Failed to insert the movie: " + err.Error()}
		} else {
			response = JsonResponse{Type: "success", Message: "The movie was inserted successfully"}
		}
	}
	json.NewEncoder(w).Encode(response)
}

func DeleteMovie(w http.ResponseWriter, r *http.Request) {
	movieName := r.FormValue("movie_name")

	var response = JsonResponse{}

	fmt.Println("Deleting movie from DB")
	if movieName == "" {
		response = JsonResponse{Type: "error", Message: "The type of MovieName and Genre is invalid"}
	} else {
		_, err := db.Exec("DELETE FROM FILM WHERE MovieName = ?", movieName)

		// check errors
		if err != nil {
			checkErr(err)
			response = JsonResponse{Type: "error", Message: "Error deleting the movie!"}
		} else {
			response = JsonResponse{Type: "success", Message: "The movie has been deleted successfully!"}
		}
	}
	json.NewEncoder(w).Encode(response)
}

func GetOneMovie(w http.ResponseWriter, r *http.Request) {
	printMessage("Get one movie...")
	movieName := r.FormValue("movie_name")

	var response = JsonResponse{}
	var movies_list []Movie

	if movieName == "" {
		response = JsonResponse{Type: "error", Message: "The type of MovieName is invalid"}
	} else {
		row := db.QueryRow("SELECT MovieName, YearIntro, Genre FROM FILM WHERE MovieName = ?", movieName)
		var movie, genre string
		var yearIntro uint64
		err := row.Scan(&movie, &yearIntro, &genre)

		MovieTmp := Movie{
			MovieName: movieName,
			YearIntro: yearIntro,
			Genre:     genre,
		}
		movies_list = append(movies_list, MovieTmp)

		if err != nil {
			if err == sql.ErrNoRows {
				http.Error(w, "Movie not found", http.StatusNotFound)
			} else {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}
	}

	response = JsonResponse{Type: "success", Data: movies_list}
	json.NewEncoder(w).Encode(response)
}

func UpdateMovie(w http.ResponseWriter, r *http.Request) {
	printMessage("Updating a movie")

	MovieName := r.FormValue("movie_name")
	YearIntro := r.FormValue("year_intro")
	Genre := r.FormValue("genre")

	var response = JsonResponse{}

	fmt.Println("Get a movie from DB")
	if MovieName == "" || YearIntro == "" || Genre == "" {
		response = JsonResponse{Type: "error", Message: "The type of MovieName is invalid"}
	} else {
		_, row := db.Exec("UPDATE FILM SET MovieName = ?, YearIntro = ?, Genre= ? WHERE MovieName = ?", MovieName, YearIntro, Genre, MovieName)

		if row != nil {
			response = JsonResponse{Type: "error", Message: "Failed to update the movie: " + row.Error()}
		} else {
			response = JsonResponse{Type: "success", Message: "The movie was updated successfully"}
		}
	}
	json.NewEncoder(w).Encode(response)
}
